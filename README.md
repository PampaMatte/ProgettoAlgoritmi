Questo è il progetto di Algoritmi

•Il progetto si svolge in gruppo (2-3 persone) e ogni gruppo sceglie un algoritmo da un elenco predefinito 

•Algoritmi su grafi pesati: 

•Albero dei cammini minimi 
1.Dijkstra
2.Johnson 
3.Bellmann-Ford-Moore 

•Minimo albero di copertura 
4.Kruskal
5.Prim


•Ogni algoritmo può essere scelto da 5 gruppi al massimo (se non bastano rilasseremo il vincolo) 