package DijkstraAlgorithm;

import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public class MyNode extends Label
{
	protected static final double NODE_SIZE = 40.0;
	
	private String name;

	public MyNode( String name )
	{
		super( name );
			setId( "MyNode" );
			setName( name );
			setPrefSize( NODE_SIZE, NODE_SIZE );
			setMinSize( NODE_SIZE, NODE_SIZE );
			setMaxSize( NODE_SIZE, NODE_SIZE );
			
		Tooltip.install( this, new Tooltip( this.getName() ) );
	}
	
	public MyNode( double x, double y, String name )
	{
		super( name );
			setId( "MyNode" );
			relocate( x, y );
			setPrefSize( NODE_SIZE, NODE_SIZE );
			setMinSize( NODE_SIZE, NODE_SIZE );
			setMaxSize( NODE_SIZE, NODE_SIZE );
		
		Tooltip.install( this, new Tooltip( this.getName() ) );
	}

	public void setName( String name ) { this.name = name; }
	
	public String getName() { return this.name;	}
}
