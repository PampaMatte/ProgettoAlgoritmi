/*
 *  ALGRAPH: requisiti
 *   • Siete liberi di scegliere le informazioni che ritenete utili per spiegare l’algoritmo ma sono richiesti:
 *     • descrizione testuale e pseudocodice dell’algoritmo
 *     • campi/bottoni per inserire dati in input e interagire con l’applicazione
 *     • strutture dati utilizzate e come queste cambiano durante l’esecuzione
 *     • ALGRAPH permette di generare il grafo di input casualmente, di importarlo da un file esterno e di modificare il grafo corrente
 *     • Informazioni e operazioni dipendono dal tipo grafo usato
 *     • ALGRAPH permette di eseguire l’algoritmo per passi o guardare il risultato finale
 *     • ALGRAPH gestisce correttamente gli errori e li notifica all’utente
 *     • input non corretto, errori di selezione, etc. 
 * --------------------------------------------------------------------------------------------------------------------------------------------
 *  Dati in input:
 *   • All’apertura ALGRAPH permette all’utente di creare il grafo su cui lanciare l’algoritmo:
 *   (1) grafo generato casualmente
 *     • l’utente deve poter scegliere il numero di nodi
 *   (2) dati caricati da un file
 *     • strutturato secondo regole stabilite dall’applicazione
 *     • ALGRAPH verifica se il file è valido ed è possibile caricare i dati; in caso contrario notifica all’utente la presenza di errori
 *   • L’utente deve poter modificare interattivamente il grafo in input
 *     • aggiungere o eliminare un nodo
 *     • modificare il peso di un arco
 *     • modificare la direzione di un arco
 *   • ALGRAPH permette di esportare il grafo corrente in un file 
 *   • I dati dipendono dal tipo di grafo usato
 *   • I pesi degli archi sono valori interi, eventualmente negativi
 *   • I nodi del grafo possono essere etichettati con numeri interi, lettere o altre informazioni purchè siano visualizzate correttamente
 *   • ALGRAPH verifica che i dati inseriti dall’utente siano corretti
 *   • E’ possibile imporre vincoli sui grafi in input (es. dimensione) ma deve essere correttamente notificato all’utente
 * ---------------------------------------------------------------------------------------------------------------------------------------------
 *  Esecuzione
 *   • ALGRAPH permette all’utente di selezionare la modalità di esecuzione e animazione dell’algoritmo:
 *     • Step-by-step: l’utente clicca su un bottone per avanzare di un “passo” nell’esecuzione
 *     • Final Result: l’utente clicca su un bottone per vedere il risultato finale
 *   • Ad ogni passo, ALGRAPH mostra le strutture dati utilizzate e come queste cambiano durante l’esecuzione
 *     • Siete liberi di scegliere messaggi di spiegazione, pop-up, animazioni o altri hints grafici 
*/
package DijkstraAlgorithm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/*  
 * Graph( String fileName )  "La stringa è il percorso del file"
 * Graph( boolean isRandom ) "Boolean per generare il grafo random(true) o il grafo vuoto(false)"
 * Graph() "Grafo random"
 *   - getIndex( Node n ) "Restituisce l'indice del nodo"
 *   - getNodeFromList( String nodename ) "Restituisce il nodo a partire dal nome"
 *   - insertNode( String nodename )
 *   - insertEdge( int peso, Node inizio, Node fine )
 *   - deleteNode(String name)
 *   
 * 
 * Dijkstra( Graph g, Node x, Node y )
 *   - goFoward() "Fa un passaggio di Dijkstra e restituisce 1[ finita con successo] -1[finita con fallimento] 0[interrotta/non finita]"
 *   - isFinished() "Controlla se Dijkstra ha finito"
 *  
 *  
 *  TODO IMPOSTARE UN VALORE NUMERICO SOTTO IL IL NOME DEL NODO PER VISUALIZZARE LA DISTANZA AL MOMENTO DAL NODO DI PARTENZA, PER OGNI PASSAGGIO DI Dijkstra.
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Screen;
import javafx.stage.Stage;              //  Stages


public class Main extends Application
{
	
	public static final int MIN_NUM_NODES = 3,
							MAX_NUM_NODES = 15,
							//MAX_NUM_EDGES = MAX_NUM_NODES / 3,
							MAX_WEIGHT = 20;

	public static final String NORMAL_PREFIX = " > ",
							   WARNING_PREFIX = "[Warning] ",
							   ERROR_PREFIX = "[!] ";

	private final double BUTTON_ROW_HEIGHT = 80,
						 CONSOLE_WIDTH = 350;

	protected static boolean isGeneratedRandomly = false,
							 isEdgesGeneratedRandomly = false;
	
	private String filePath;
	
	public Graph graph = null;
	
	protected Stage dijkstraStage;
	protected Scene dijkstraScene;
	protected HBox row2;
	protected HBox row4;
	
	protected Stage stage;
	protected Scene scene;
	protected ScrollPane rightPanel;
	protected static VBox console;

	protected static Pane mainPanel;
	protected MyButton btnInsertNode, btnDeleteNode, btnInsertEdge,btnDeleteEdge, btnGoFoward, btnGoToEnd, btnReload;

	private Dijkstra dijkstra = null;

	public static void main( String[] args )
	{
		launch( args );
	}
	
	public void start( Stage stage )
	{
		console = new VBox();
		console.setId( "console" );
		runFirstDialogue( stage, 400, 150 );
	}
  
	/**
	 * Funzione per la stampa sulla console dell'applicazione
	 * @param prefix: prefisso definito del messaggio
	 * @param output: corpo effettivo del messaggio
	 * @param endline: possibile inserimento di stringhe come fine riga
	 * @param errorCode: codice di errore che determina il colore del messaggio
	 */
	public static void printErrConsole( String prefix, String output, String endline, int errorCode )
	{
		try
		{
			Label tmp = new Label( prefix + output + endline );
			switch ( errorCode )
			{
				case 100:
					tmp.setStyle( "-fx-text-fill: #FFF;" ); break;    //  Messaggio normale, testo di colore bianco (va abbinato a Main.NORMAL_PREFIX)
				case 200:
					tmp.setStyle( "-fx-text-fill: #4CAF50;" ); break; //  Messaggio di successo, testo di colore verde (va abbinato a Main.NORMAL_PREFIX)
				case 300:
					tmp.setStyle( "-fx-text-fill: #FFC107;" ); break; //  Messaggio di warning, testo di colore arancione (va abbinato a Main.WARNING_PREFIX)
				case 400:
					tmp.setStyle( "-fx-text-fill: #F44336;" ); break; //  Messaggio di errore, testo di colore rosso (va abbinato a Main.ERROR_PREFIX)
			}
			Main.console.getChildren().add( tmp );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}

	private void runFirstDialogue( Stage stage, double width, double height )
	{
		// =============================>
		//runSingleTest();
		// =============================>
		//runGenerationTest();
		//Main.printErrConsole( "", "-----------------------------------", "", 300 );
		// =============================>
		this.stage = stage;
		
		this.stage.setTitle( "WELCOME TO ALGRAPH!" );

		Rectangle2D mainScreen = Screen.getPrimary().getVisualBounds();

		this.stage.setX( ( mainScreen.getWidth() - width ) / 2.0 );
		this.stage.setY( ( mainScreen.getHeight() - height ) / 2.0 );
		this.stage.setWidth( /*mainScreen.getWidth()*/ width );
		this.stage.setHeight( /*mainScreen.getHeight()*/ height );

		BorderPane root = new BorderPane();
		root.setId( "firstDialog" );

		scene = new Scene( root, 1, 1 );
		scene.getStylesheets().add( "DijkstraAlgorithm/main-window.css" );
		this.stage.setScene( scene );
		
		Label tag = new Label( "Scegli come vuoi che sia generato il grafo:" );
		tag.setPrefWidth( width );
		tag.setPrefHeight( 30 );
		tag.setId( "firstDialog-tag" );
		root.setCenter( tag );
		
		HBox horizontal = new HBox();
		horizontal.setPrefWidth( width );
		horizontal.setPrefHeight( 60 );
		horizontal.setId( "firstDialog-choicheButtons" );
		root.setBottom( horizontal );
		
		MyButton choice1 = new MyButton( "Carica file" ),
				 choice2 = new MyButton( "Vuoto" ),
				 choice3 = new MyButton( "Random" );
		
		choice1.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				runFileExplorer();
			}
		} );
		
		choice2.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				graph = new Graph( false );
				runApplication( graph );
			}
		} );
		
		choice3.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				graph = new Graph( true );
				Main.isGeneratedRandomly = true;
				runApplication( graph );
			}
		} );
		
		
		horizontal.getChildren().addAll( choice1, choice2, choice3 );
		
		this.stage.setResizable( false );
		this.stage.show();
	}
	
	private void runFileExplorer()
	{
		FileChooser fileChooser = new FileChooser();
		
		// Imposto il fileChooser in modo che accetti solo la nostra estensione .gph
		ExtensionFilter filter = new ExtensionFilter( "Graph file", "*." + Graph.EXT );
		fileChooser.getExtensionFilters().add( filter );
		fileChooser.setSelectedExtensionFilter( filter );
		fileChooser.setTitle( "Apri file con il grafo" );
		
		try
		{
			filePath = fileChooser.showOpenDialog( this.stage ).getAbsolutePath();
			// ==================================================>
			System.out.println( "Percorso file ==> " + filePath );
			// ==================================================>
		}
		catch( NullPointerException e )
		{
			return;
		}
		
		try
		{
			graph = new Graph( filePath );
			runApplication( graph );
		}
		catch ( IOException e )  // Gestisco l'eccezione dell'apertura del file
		{
			Alert alert = new Alert( AlertType.ERROR );
			alert.setTitle( "Errore" );
			alert.setHeaderText( "Fatal error" );
			alert.setContentText( "È stato impossibile aprire il file in posizione: " + filePath );
			alert.showAndWait();
			
			graph = new Graph( false );
			runApplication( graph );
			Main.printErrConsole( Main.ERROR_PREFIX, "Si è verificato un errore nell'apertura del file.", "", 400 );
			
			e.printStackTrace();
		}
		
	}
	
	private void runApplication( Graph graph )
	{
		this.stage.setTitle( "ALGRAPH" );

		Rectangle2D mainScreen = Screen.getPrimary().getVisualBounds();

		this.stage.setX( mainScreen.getMinX() );
		this.stage.setY( mainScreen.getMinY() );
		this.stage.setWidth( mainScreen.getWidth() );
		this.stage.setHeight( mainScreen.getHeight() );

		HBox root = new HBox();
		//root.getChildren().add(element);
		//Font.loadFont( Main.class.getResource( "Roboto.ttf" ).toExternalForm(), 10 );
		scene = new Scene( root, 1, 1 );
		scene.getStylesheets().add( "DijkstraAlgorithm/main-window.css" );
		this.stage.setScene( scene );

		VBox divTop = new VBox();
		root.getChildren().add( divTop );

	//  MAIN PANEL ==================================================================
		mainPanel = new Pane();
		mainPanel.setPrefSize( this.stage.getWidth() - CONSOLE_WIDTH, this.stage.getHeight() - BUTTON_ROW_HEIGHT );
		mainPanel.setId( "mainPanel" );
		//root.setCenter( mainPanel );
		divTop.getChildren().add( mainPanel );

	//  BOTTOM PANEL ================================================================
		btnInsertNode = new MyButton( "INSERISCI NODO" );
		btnDeleteNode = new MyButton( "ELEMINA NODO" );
		btnInsertEdge = new MyButton( "INSERISCI ARCO" );
		btnDeleteEdge = new MyButton( "ELIMINA ARCO" );
		btnGoFoward = new MyButton( "AVANZA" );
		btnGoToEnd = new MyButton( "AVANZA TUTTO" );
		btnReload = new MyButton( "RICARICA" );

		FlowPane bottomPanel = new FlowPane();
		bottomPanel.setId( "bottomPanel" );
		bottomPanel.setPrefSize( this.stage.getWidth() - CONSOLE_WIDTH, BUTTON_ROW_HEIGHT );

		bottomPanel.getChildren().addAll( 
				btnInsertNode,
				btnDeleteNode,
				btnInsertEdge,
				btnDeleteEdge,
				btnGoFoward,
				btnGoToEnd,
				btnReload
		);

		//root.setBottom( bottomPanel );
		divTop.getChildren().add( bottomPanel );
		//BOTTONE "AGGIUNGI NODO"
		btnInsertNode.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				//  Controllo se non è stato raggiunto il numero massimo di nodi
				if ( graph.getAllNodes().size() != Main.MAX_NUM_NODES )
				{
					/** FINESTRA CON AGGIUNTA DEL NUOVO NODO **/
					TextInputDialog dialog = new TextInputDialog( Integer.toString( graph.getAllNodes().size() + 1 ) );
					dialog.setTitle( "Inserimento nuovo nodo" );
					dialog.setContentText( "Inserisci il nome del nuovo nodo:" );
	
					Optional<String> result = dialog.showAndWait();
						
					if ( result.get() != null && !result.get().isEmpty() )
					{
						if ( graph.getNodeFromList( result.get() ) == null )
						{
								graph.insertNode( result.get() );
								refreshPanel( graph, graph.getAllNodes().size() );
								Main.printErrConsole( Main.NORMAL_PREFIX, "Aggiunto nodo: " + result.get(), "", 200 );
							
						}
						else
							Main.printErrConsole( Main.WARNING_PREFIX, "Il nome del nodo è già esistente", "", 300 );
					}
					else
						Main.printErrConsole( Main.ERROR_PREFIX, "Il nome del nodo non può essere vuoto", "", 400 );
				}
				else  //  Il numero di nodi ha raggiunto il massimo
				{
					Main.printErrConsole( Main.WARNING_PREFIX, Main.MAX_NUM_NODES + " è il numero massimo di nodi", "", 300 );
					Main.printErrConsole( Main.ERROR_PREFIX, "Impossibile aggiungere nodo", "", 400 );
				}
			}
		} );
		//BOTTONE "ELIMINA NODO"
		btnDeleteNode.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
			//  Controllo se non è stato raggiunto 0 come numero di nodi
				if ( graph.getAllNodes().size() > 0 )
				{
					/** FINESTRA CON ELIMINAZIONE DI UN NODO **/
					List<String> choices = new ArrayList<>();
					for( int i = 0; i < graph.getAllNodes().size(); i++ )
						choices.add( graph.getAllNodes().get( i ).getName() );

					ChoiceDialog<String> dialog = new ChoiceDialog<>( choices.get( 0 ), choices );
					dialog.setTitle( "Eliminazione di un nodo" );
					dialog.setContentText( "Seleziona il nodo da eliminare:" );

					// Traditional way to get the response value.
					Optional<String> result = dialog.showAndWait();
					if ( result.isPresent() )
					{
						if ( graph.getIndexOf( result.get() ) != -1 )
						{
							//int index = graph.getIndexOf( result.get() );
							MyNode deleted = graph.getNodeFromList( result.get() );
							//graph.getAllNodes().remove( index );*/
							
							
							//Main.printErrConsole( Main.NORMAL_PREFIX, "Nodo " + result.get() + " eliminato con successo", "", 200 );
							if ( graph.getAllEdges().size() > 0 )
							{
								//  Cerco se il nodo eliminato fa parte di un arco
								// ==> PS: il limite dell'indice va ricalcolato a ogni iterazione per eliminazioni di archi multiple
								for ( int i = 0; i < graph.getAllEdges().size(); i++ )
								{
									MyEdge tmp = graph.getAllEdges().get( i );
									//System.out.println( "Analizzato arco da [" + tmp.getStartNode().getName() + "] a [" + tmp.getEndNode().getName() + "]" );
									
									//  Se il nodo eliminato è un nodo di entrata o di uscita di un arco lo elimino
									if ( deleted.getName().equals( tmp.getStartNode().getName() ) )
									{	
										//System.out.println( "[" + i + "] Eliminazione arco [" + tmp.getStartNode().getName() +  "][" + tmp.getEndNode().getName() + "]");
										graph.deleteEdge( deleted.getName(), tmp.getEndNode().getName() );
										i--;
									}
									else if ( deleted.getName().equals( tmp.getEndNode().getName() ) )
									{
										//System.out.println( "[" + i + "] Eliminazione arco [" + tmp.getStartNode().getName() +  "][" + tmp.getEndNode().getName() + "]");
										graph.deleteEdge( tmp.getStartNode().getName() , deleted.getName() );
										i--;
									}
								}
							}
							// Elimino effettivamente il nodo
							graph.deleteNode( result.get() );
						}
						else
							Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 );
					}
					refreshPanel( graph, graph.getAllNodes().size() );
				}
				else  //  I nodi presenti nel grafo sono 0
					Main.printErrConsole( Main.ERROR_PREFIX, "Non sono presenti alcuni nodi da eliminare", "", 400 );
			}
		} );
		//BOTTONE "AGGIUNGI ARCO"
		btnInsertEdge.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				/**
				 * FINESTRA CON AGGIUNTA DEL NUOVO ARCO 
				 */
				if ( graph.getAllNodes().size() >= 2 )
				{
					MyNode startNode,
						   endNode;
					
					/** Selezione nodo di partenza **/
					List<String> choices = new ArrayList<>();
					for( int i = 0; i < graph.getAllNodes().size(); i++ )
						choices.add( graph.getAllNodes().get( i ).getName() );

					ChoiceDialog<String> dialog = new ChoiceDialog<>( choices.get( 0 ), choices );
					dialog.setTitle( "Inserimento nuovo arco" );
					dialog.setHeaderText("Seleziona il nodo di partenza");
					
					Optional<String> result = dialog.showAndWait();
					
					if ( result.isPresent() )
					{
						if ( graph.getIndexOf( result.get() ) != -1 )
						{
							startNode = graph.getNodeFromList( result.get() ); 
							
							/** Selezione nodo di arrivo **/
							dialog.setHeaderText("Seleziona il nodo di arrivo");
							
							result = dialog.showAndWait();
							
							if ( result.isPresent() )
							{
								if ( graph.getIndexOf( result.get() ) != -1 )
								{
									
									endNode = graph.getNodeFromList( result.get() );      
									
									if ( !startNode.equals( endNode ) )
									{
										/** Inserimento del peso **/
										TextInputDialog weigthDialog = new TextInputDialog( "1" );
										weigthDialog.setTitle( "Inserimento nuovo arco" );
										weigthDialog.setContentText("Peso dell'arco");

										Optional<String> weigth = weigthDialog.showAndWait();
										if ( weigth.isPresent() )
										{
											/** Controllo il peso inserito **/
											if ( Graph.isInteger( weigth.get() ) )
											{
												/** Controllo se l'arco esiste già **/
												boolean alreadyPresent = false;
												int size = graph.getAllEdges().size();
												for( int i = 0; i < size; i++ )
												{
													if ( ( startNode == graph.getAllEdges().get( i ).getStartNode() && endNode == graph.getAllEdges().get( i ).getEndNode() )
													  || ( startNode == graph.getAllEdges().get( i ).getEndNode() && endNode == graph.getAllEdges().get( i ).getStartNode() ) )
														alreadyPresent = true;
												}
												
												if ( !alreadyPresent )
												{
													/** Aggiungo il nuovo arco **/
													graph.insertEdge( startNode.getName(), endNode.getName(), Integer.parseInt( weigth.get() )  ); 
													refreshPanel( graph, graph.getAllNodes().size() );
												}
												else
													Main.printErrConsole( Main.WARNING_PREFIX, "L'arco inserito è già presente nel grafo", "", 300 ); 
												
											}
											else
												Main.printErrConsole( Main.ERROR_PREFIX, "Il peso dell'arco non è corretto", "", 400 ); 
										}
									}
									else
										Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo di partenza di un arco e il suo nodo di arrivo non possono essere uguali", "", 400 );
								}
								else
									Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 ); 
							}
						}
						else
							Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 );
					}
				}
				else
					Main.printErrConsole( Main.ERROR_PREFIX, "Non sono presenti abbastanza nodi per un arco", "", 400 );
				
				/**
				 * 
				 */
			}
		} );
		
		/**
		 * Elimina arco
		 */
		btnDeleteEdge.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				/**
				 * FINESTRA CON ELIMINAZIONE DELL'ARCO 
				 */
				if ( graph.getAllEdges().size() > 0 )
				{
					MyNode startNode,
						   endNode;
					
					/** Selezione nodo di partenza **/
					List<String> choices = new ArrayList<>();
					for( int i = 0; i < graph.getAllNodes().size(); i++ )
						choices.add( graph.getAllNodes().get( i ).getName() );

					ChoiceDialog<String> dialog = new ChoiceDialog<>( choices.get( 0 ), choices );
					dialog.setTitle( "Eliminazione arco" );
					dialog.setHeaderText("Seleziona il nodo di partenza");
					
					Optional<String> result = dialog.showAndWait();
					
					if ( result.isPresent() )
					{
						if ( graph.getIndexOf( result.get() ) != -1 )
						{
							startNode = graph.getNodeFromList( result.get() ); 
							
							/** Selezione nodo di arrivo **/
							dialog.setHeaderText( "Seleziona il nodo di arrivo" );
							result = dialog.showAndWait();
							
							if ( result.isPresent() )
							{
								if ( graph.getIndexOf( result.get() ) != -1 )
								{
									endNode = graph.getNodeFromList( result.get() );
									
									/** Controllo se l'arco esiste **/
									boolean edgeIn = false,
											edgeOut = false;
									int size = graph.getAllEdges().size();
									for( int i = 0; i < size; i++ )
									{
										if ( startNode == graph.getAllEdges().get( i ).getStartNode() && endNode == graph.getAllEdges().get( i ).getEndNode() )
											edgeIn = true;
										
										if( startNode == graph.getAllEdges().get( i ).getEndNode() && endNode == graph.getAllEdges().get( i ).getStartNode() )
											edgeOut = true;
									}
									
									if ( edgeIn )
									{
										/** Elimino l'arco entrante **/
										graph.deleteEdge( startNode.getName(), endNode.getName() );
										//System.out.println( "Entrante" );
									}
									else if ( edgeOut )
									{
										/** Elimino l'arco uscente **/
										graph.deleteEdge( endNode.getName(), startNode.getName() );
										//System.out.println( "Uscente" );
									}
									else
										Main.printErrConsole( Main.WARNING_PREFIX, "L'arco inserito non esiste", "", 300 );
									
									refreshPanel( graph, graph.getAllNodes().size() );
								}
								else
									Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 ); 
							}
						}
						else
							Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 );
					}
				}
				else
					Main.printErrConsole( Main.ERROR_PREFIX, "Non sono presenti archi nel grafo", "", 400 );
				/**
				 * 
				 */
			}
		} );
		
		//BOTTONE "AVANZA"
		btnGoFoward.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				/**
				 * L'ALGORITMO DI DIJKSTRA AVANZA DI UN PASSAGGIO
				 */
				if ( dijkstra == null )
					dijkstraSettingsPanel();
				
				if ( dijkstra != null )
				{	
					displayStepDijkstra ();
				}
			}
		} );
		//BOTTONE "AVANZA TUTTO"
		btnGoToEnd.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				/** L'ALGORITMO DI DIJKSTRA AVANZA FINO A TERMINARE **/
				if ( dijkstra == null )
					dijkstraSettingsPanel();
				
				if ( dijkstra != null )
				{
					int finish = 0;
					while ( finish == 0 )
						finish = displayStepDijkstra();
				}
			}
		} );
		
		//BOTTONE "RICARICA"
		btnReload.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override public void handle( ActionEvent e )
			{
				try
				{
					/** Ripristino il colore dell'ultimo arco visitato **/
					if( null != dijkstra.getLastEdgeVisited() )
						dijkstra.getLastEdgeVisited().getLine().setStyle( "-fx-stroke: #263238;" );
				}
				catch( NullPointerException npe )
				{
					logWrite( "Errore nel ricaricare il grafo \n" + npe.getMessage() );
				}
				/** L'ALGORITMO DI DIJKSTRA VIENE AZZERATO **/
				dijkstra = null;
				
				repaintGraph();
				
				/** Abilito i bottoni **/
				btnInsertNode.setDisable( false );
				btnDeleteNode.setDisable( false );
				btnInsertEdge.setDisable( false );
				btnDeleteEdge.setDisable( false );
				
				try
				{
					/** Chiudo finestra Dijkstra **/
					dijkstraStage.close();
				}
				catch( NullPointerException npe )
				{
					
				}
				
				dijkstraStage = null;
				dijkstraScene = null;
				
				
			}
		} );

	//  RIGHT PANEL =================================================================
		rightPanel = new ScrollPane();
		rightPanel.setId( "rightPanel" );

		/**
		 * Inserisco nel scroll panel la console
		 */
		rightPanel.setContent( console );
		rightPanel.setPrefSize( 350, this.stage.getHeight() );
		//root.setRight( rightPanel );
		root.getChildren().add( rightPanel );
		
		//  Mostro la finesrra principale
		this.stage.show();
		
		//  Messaggi di benvenuto e inizializzazione
		Main.printErrConsole(  Main.NORMAL_PREFIX, "Benvenuto!", " ", 100 );
		
		if ( graph.getAllNodes().size() != 0 )
			Main.printErrConsole( Main.NORMAL_PREFIX, "Grafo generato con " + graph.getAllNodes().size() + " nodi", "", 100 );
		else
			Main.printErrConsole( Main.NORMAL_PREFIX, "Grafo vuoto generato", "", 100 );
		

		
		if ( filePath != null )
			Main.printErrConsole( "   ", "Caricato file in posizione: " + filePath, "", 200 );
		
		/*Main.printStrConsole( "   ", "Benvenuto!", " " );
		Main.printErrConsole( Main.NORMAL_PREFIX, "Corretto!", " ", 200 );
		Main.printErrConsole( Main.WARNING_PREFIX, "Warning!", " ", 300 );
		Main.printErrConsole( Main.ERROR_PREFIX, "Errore", " ", 400 );*/

		//  Se il grafo è stato generato in modo random (per i nodi) allora genero random anche gli archi
		if ( Main.isGeneratedRandomly && !Main.isEdgesGeneratedRandomly )
		{
			graph.generateRandomEdges();
			Main.isEdgesGeneratedRandomly = true;
		}
		
		refreshPanel( graph, graph.getAllNodes().size() );
		Main.printErrConsole( Main.NORMAL_PREFIX, "Caricamento completato...", "", 200 );
	}
	
	/**
	 * Ristampa della parte grafica dell'applicazione principale
	 * @param g: grafo in input
	 * @param nodeNumber: numero di nodi
	 */
	private void refreshPanel( Graph g, int nodeNumber )
	{
		Main.mainPanel.getChildren().clear();  //  Elimino tutti i vecchi nodi dal pannello
		
		final int STD_RADIUS = 40,
				  MIN_DISTANCE = 150,
				  NUM_ARCS = 5,
				  ARC_FACTOR = 10;

		final double PADDING = 10.0;

		int radius = MIN_DISTANCE + ( NUM_ARCS * ARC_FACTOR );
		
		//  Coordinate del centro del pannello
		double centerX = Main.mainPanel.getPrefWidth() / 2 + PADDING, 
			   centerY = Main.mainPanel.getPrefHeight() / 2 + PADDING;

		double angleArcSections = ( 360.0 / nodeNumber );
		double actualAngle = angleArcSections;
		
		for ( int i = 0; i < nodeNumber; i++ )
		{
			//  Coseno e seno dell'angolo calcolato 
			double cos = Math.cos( Math.toRadians( actualAngle ) ) * radius,
				   sin = Math.sin( Math.toRadians( actualAngle ) ) * radius;

			//  X del centro della circonferenza + coseno dell'angolo - la metà del raggio | Y del centro della circonferenza + seno dell'angolo - la metà del raggio
			g.getAllNodes().get( i ).relocate( centerX + cos - ( Math.round( STD_RADIUS / 2.0 ) ), centerY + sin - ( Math.round( STD_RADIUS / 2.0 ) ) );
			actualAngle += angleArcSections;
		}
		
		//  Stampo a schermo le linee degli archi
		for ( int i = 0; i < g.getAllEdges().size(); i++ )
		{
			MyEdge tmp = g.getAllEdges().get( i );
			tmp.repaint();
			Main.mainPanel.getChildren().add( tmp.getLine() );
		}
		
		//  Stampo a schermo le labels dei pesi degli archi
		for ( int i = 0; i < g.getAllEdges().size(); i++ )
		{
			MyEdge tmp = g.getAllEdges().get( i );
			tmp.repaint();
			Main.mainPanel.getChildren().add( tmp.getWeightLabel() );
		}

		for ( int i = 0; i < nodeNumber; i++ )
			Main.mainPanel.getChildren().add( g.getAllNodes().get( i ) );
	}

	//creo pannello per inizializzare DIJKSTRA e lo iniziallizzo se rispetta condizioni necessarie
	private void dijkstraSettingsPanel () 
	{
		if ( graph.getAllNodes().size() > 1 )
		{
			MyNode indexStart,
				   indexDest;
			
			/** FINESTRA SELEZIONE NODO DI ARRIVO**/
			List<String> choices = new ArrayList<>();
			for( int i = 0; i < graph.getAllNodes().size(); i++ )
				choices.add( graph.getAllNodes().get( i ).getName() );

			ChoiceDialog<String> dialog = new ChoiceDialog<>( choices.get( 0 ), choices );
			dialog.setTitle( "Impostazioni" );
			dialog.setHeaderText("Seleziona il nodo di partenza");
			
			Optional<String> result = dialog.showAndWait();
			
			if ( result.isPresent() )
			{
				if ( graph.getIndexOf( result.get() ) != -1 )
				{
					indexStart = graph.getNodeFromList( result.get() ); 
					
					/** FINESTRA SELEZIONE NODO ARRIVO **/
					
					dialog.setTitle("Impostazioni");
					dialog.setHeaderText("Seleziona il nodo di arrivo");
					
					result = dialog.showAndWait();
					
					if ( result.isPresent() )
					{
						if ( graph.getIndexOf( result.get() ) != -1 )
						{
							indexDest = graph.getNodeFromList( result.get() );      
							
							/** CREO L'OGGETTO DIJKSTRA **/
							this.dijkstra = new Dijkstra ( graph , indexStart , indexDest );
							
							/** Disabilito i bottoni **/
							btnInsertNode.setDisable( true );
							btnDeleteNode.setDisable( true );
							btnInsertEdge.setDisable( true );
							btnDeleteEdge.setDisable( true );
							
							/** Creo la finestra con le informazioni **/
							runExternWindow ( 700, 350 );
						}
						else
							Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 ); 
					}
				}
				else
					Main.printErrConsole( Main.ERROR_PREFIX, "Nodo non trovato", "", 400 );
				
			}
			
			
		}
		else  //  I nodi presenti nel grafo sono 0 o 1 
			Main.printErrConsole( Main.ERROR_PREFIX, "Numero di nodi insufficiente", "", 400 );
		
	}

	private int displayStepDijkstra ()
	{
		/** 
		 * Ripristino il colore dell'arco visitato precedentemente 
		 */
		if( null != dijkstra.getLastEdgeVisited() && !dijkstra.isFinished() )
			dijkstra.getLastEdgeVisited().getLine().setStyle( "-fx-stroke: #263238;" );
		
		//avanza dijkstra e mostra le modifiche
		int resultDijkstra = dijkstra.goForward();
		refreshDijkstraPanel();
		
		/**
		 * Coloro l'ultimo nodo visitato
		 */
		if ( null != dijkstra.getLastNodeVisited() )
			dijkstra.getLastNodeVisited().setStyle( "-fx-background-color: #F44336;"
		                                      + "-fx-border-width: 3;" 
		                                      + "-fx-border-radius: 50%;" 
		                                      + "-fx-border-color: #F44336;" );
		/** 
		 * Coloro l'arco visitato
		 */
		if( null != dijkstra.getLastEdgeVisited() )
			dijkstra.getLastEdgeVisited().getLine().setStyle( "-fx-stroke: #F44336;" );
			
		//stampa sul terminale alcune informazioni sullo stato dell'algoritmo
		if ( resultDijkstra == 2 )
			Main.printErrConsole( Main.NORMAL_PREFIX, "L'algoritmo ha portato a termine il suo scopo", "", 100 );
		else if ( resultDijkstra == -1 )
			Main.printErrConsole( Main.NORMAL_PREFIX, "Il percorso per arrivare da " + dijkstra.getStart().getName() + " a " + dijkstra.getDestination().getName() + " è inesistente.", "", 100 );
		else if ( resultDijkstra == 1 )
		{
			Main.printErrConsole( Main.NORMAL_PREFIX, "Nodo visitato: " + dijkstra.getLastNodeVisited().getName(), "", 100 );
			Main.printErrConsole( Main.NORMAL_PREFIX, "La ricerca del miglior percorso si è conclusa con successo!", "", 100 );
			
			repaintGraph();
			
			/** Ripristino il colore dell'ultimo arco visitato **/
			if( null != dijkstra.getLastEdgeVisited() )
				dijkstra.getLastEdgeVisited().getLine().setStyle( "-fx-stroke: #263238;" );
			
			/** Viene colorato il percorso migliore **/
			paintBestPath();
		}
		else
		{
			//stampo tutti i passaggi fatti e le modifiche dell'algoritmo
			Main.printErrConsole( Main.NORMAL_PREFIX, "Nodo visitato: " + dijkstra.getLastNodeVisited().getName(), "", 100 );
		}
			
		return resultDijkstra;
	}
	
	/**
	 * Ripristina i colori base dei nodi e degli archi del grafo
	 */
	private void repaintGraph()
	{
		ArrayList<MyNode> nodesList = graph.getAllNodes();
		ArrayList<MyEdge> edgesList = graph.getAllEdges();
		
		int nodesNumber = nodesList.size(),
		    edgesNumber = edgesList.size();
		
		/** Ripristino i colori di tutti i nodi **/
		for ( int i = 0; i < nodesNumber; i++ )
			nodesList.get( i ).setStyle( "-fx-background-color: #263238;"
		                              + "-fx-border-width: 3;"
		                              + "-fx-border-radius: 50%;"
		                              + "-fx-border-color: #263238;" );
		
		/** Ripristino i colori di tutti gli archi **/
		for ( int i = 0; i < edgesNumber; i++ )
			edgesList.get( i ).getLine().setStyle( "-fx-stroke: #263238;" );
	}
	
	//Finestra in cui vengono stampati gli array "bestPredecessor" e "DistanceToRoot" della classe Dijkstra
	private void runExternWindow (double width, double height) {
		
		dijkstraStage = new Stage();
		dijkstraStage.setTitle( "INFORMAZIONI DIJKSTRA" );
		
		dijkstraStage.setX( 0 );
		dijkstraStage.setY( 0 );
		
		dijkstraStage.setWidth( width );
		dijkstraStage.setHeight( height );
		
		BorderPane root = new BorderPane();
		
		dijkstraScene = new Scene( root );
		dijkstraScene.getStylesheets().add( "DijkstraAlgorithm/main-window.css" ); //da modificare il nome del file css
		dijkstraStage.setScene( dijkstraScene );
		
		// PRIMO ARRAY: bestPredecessor
		BorderPane bestPredContainer = new BorderPane ();
		bestPredContainer.setId( "container" );
		
		Label tag1 = new Label( "Miglior predecessore di ogni nodo:" );
		tag1.setPrefWidth( width );
		tag1.setPrefHeight( 30 );
		tag1.setId( "firstDialog-tag" );
		bestPredContainer.setTop( tag1 );
		
		VBox vertical1 = new VBox ();
		vertical1.setPrefSize( width , 120);
		
		HBox row1 = new HBox();
		row1.setPrefWidth( width );
		row1.setPrefHeight( 60 );
		row1.setId( "row-table" );
		
		row2 = new HBox();
		row2.setPrefWidth( width );
		row2.setPrefHeight( 60 );
		row2.setId( "row-table" );
		
		// SECONDO ARRAY: DistanceToRoot
		
		BorderPane distRootContainer = new BorderPane ();
		bestPredContainer.setId( "container" );
		
		Label tag2 = new Label( "Distanza dalla radice di ogni nodo:" );
		tag2.setPrefWidth( width );
		tag2.setPrefHeight( 30 );
		tag2.setId( "firstDialog-tag" );
		distRootContainer.setTop( tag2 );
		
		VBox vertical2 = new VBox ();
		vertical2.setPrefSize( width , 120);
		
		HBox row3 = new HBox();
		row3.setPrefWidth( width );
		row3.setPrefHeight( 60 );
		row3.setId( "row-table" );
		
		row4 = new HBox();
		row4.setPrefWidth( width );
		row4.setPrefHeight( 60 );
		row4.setId( "row-table" );
			
		//assegno i valori alle label e le metto nei rispettivi HBOX
		
		ArrayList<MyNode> nodes = graph.getAllNodes();
		HashMap<MyNode, MyNode> bestPred = dijkstra.getBestPredecessor();
		HashMap<MyNode, Integer> distRoot = dijkstra.getDistanceToRoot();
		
		for (int i = 0; i < nodes.size(); i++ ) {
			
			Label tmp1 = new Label ( nodes.get(i).getName() ),
				  tmp4 = new Label ( nodes.get(i).getName() ),
				  tmp2,
				  tmp3;
			
			if (bestPred.get(nodes.get(i)) == null )
				tmp2 = new Label ( "?" );
			else
				tmp2 = new Label ( bestPred.get(nodes.get(i)).getName() );
			
			if ( distRoot.get(nodes.get(i)) == Integer.MAX_VALUE )
				tmp3 = new Label ( "∞" );
			else
				tmp3 = new Label ( distRoot.get(nodes.get(i)).toString() );
			
			tmp1.setId( "table-value" );
			tmp2.setId( "table-value" );
			tmp3.setId( "table-value" );
			tmp4.setId( "table-value" );
			
			row1.getChildren().add( tmp1 );
			row2.getChildren().add( tmp2 );
			row3.getChildren().add( tmp4 );
			row4.getChildren().add( tmp3 );
			
		}

		vertical1.getChildren().addAll( row1, row2 );
		bestPredContainer.setCenter( vertical1 );
		
		vertical2.getChildren().addAll( row3, row4 );
		distRootContainer.setCenter( vertical2 );
		
		root.setCenter( bestPredContainer );
		root.setBottom( distRootContainer );
		
		dijkstraStage.setResizable( false );
		dijkstraStage.show();
	}
	
	//aggiorna la finestra che mostra le informazioni di dijkstra
	private void refreshDijkstraPanel () {
		
		row2.getChildren().clear();
		row4.getChildren().clear();
		
		//assegna i valori alle label e le mette nei rispettivi HBOX
		ArrayList<MyNode> nodes = graph.getAllNodes();
		HashMap<MyNode, MyNode> bestPred = dijkstra.getBestPredecessor();
		HashMap<MyNode, Integer> distRoot = dijkstra.getDistanceToRoot();
		
		for (int i = 0; i < nodes.size(); i++ ) {
			
			Label tmp2,
				  tmp3;
			
			if (bestPred.get(nodes.get(i)) == null )
				tmp2 = new Label ( "?" );
			else
				tmp2 = new Label ( bestPred.get(nodes.get(i)).getName() );
			
			if ( distRoot.get(nodes.get(i)) == Integer.MAX_VALUE )
				tmp3 = new Label ( "∞" );
			else
				tmp3 = new Label ( distRoot.get(nodes.get(i)).toString() );
	
			tmp2.setId( "table-value" );
			tmp3.setId( "table-value" );
			
			row2.getChildren().add( tmp2 );
			row4.getChildren().add( tmp3 );
			
		}	
	}
	
	/**
	 * Colora il percorso migliore del grafo trovato dall'algoritmo
	 */
	private void paintBestPath()
	{
		ArrayList<MyNode> bestPath = dijkstra.getBestPath();
		int pathLength = bestPath.size();
		
		for ( int i = pathLength - 2; i >= 0; i-- )
		{
			MyNode actualNode = bestPath.get( i );
			if ( null != actualNode )
				actualNode.setStyle( "-fx-background-color: #4CAF50;"
		                           + "-fx-border-width: 3;"
		                           + "-fx-border-radius: 50%;"
		                           + "-fx-border-color: #4CAF50;" );
			
			MyEdge actualEdge = graph.getEdge( actualNode, bestPath.get( i + 1 ) );
			if ( null != actualEdge )
				actualEdge.getLine().setStyle( "-fx-stroke: #4CAF50;" );
		}
	}
	
	
	/**
	 * SEZIONE DI TESTING
	 */
	public void runGenerationTest()
	{
		int fallimenti = 0;
		int cicli = 100;
		
		for( int i = 0; i < cicli; i++ )
		{
			Graph g;
			try
			{
				g = new Graph( "/mnt/5995687E006F3A0D/Privato/GIT/ProgettoAlgoritmi/DijkstraAlgorithm/test." + Graph.EXT );
			}
			catch( Exception e )
			{
				fallimenti++;
				System.out.println( "Fallimento n° " + fallimenti );
				e.printStackTrace();
			}
		}
		
		for( int i = 0; i < cicli; i++ )
		{
			Graph g;
			try
			{	
				g = new Graph( "/mnt/5995687E006F3A0D/Privato/GIT/ProgettoAlgoritmi/DijkstraAlgorithm/test2." + Graph.EXT );
			}
			catch( Exception e )
			{
				fallimenti++;
				System.out.println( "Fallimento n° " + fallimenti );
				e.printStackTrace();
			}
		}

		for( int i = 0; i < cicli; i++ )
		{
			Graph g;
			try
			{
				
				if ( i % 2 == 0 )
					g = new Graph( true );
				else
					g = new Graph( false );
			}
			catch( Exception e )
			{
				fallimenti++;
				System.out.println( "Fallimento n° " + fallimenti );
				e.printStackTrace();
			}
		}
		
		System.out.println( "Test su " + cicli +" cicli terminato con " + fallimenti + " errori" );
		
	}
	
	public void runSingleTest()
	{
		int fallimenti = 0;
		Graph g;
		
		try
		{
			g = new Graph( "/mnt/5995687E006F3A0D/Privato/GIT/ProgettoAlgoritmi/DijkstraAlgorithm/test." + Graph.EXT );
		}
		catch( Exception e )
		{
			writeError( e, ++fallimenti );
			System.out.println( "Fallimento n° " + fallimenti );
			//e.printStackTrace();
		}
	
		try
		{	
			g = new Graph( "/mnt/5995687E006F3A0D/Privato/GIT/ProgettoAlgoritmi/DijkstraAlgorithm/test2." + Graph.EXT );
		}
		catch( Exception e )
		{
			writeError( e, ++fallimenti );
			System.out.println( "Fallimento n° " + fallimenti );
			//e.printStackTrace();
		}
	
		try
		{
				g = new Graph( true );
		}
		catch( Exception e )
		{
			writeError( e, ++fallimenti );
			System.out.println( "Fallimento n° " + fallimenti );
			//e.printStackTrace();
		}
		
		try
		{
				g = new Graph( false );
		}
		catch( Exception e )
		{
			writeError( e, ++fallimenti );
			System.out.println( "Fallimento n° " + fallimenti );
			//e.printStackTrace();
		}
		
		System.out.println( "SingleTest() terminato con " + fallimenti + " errori" );
		
	}
	
	public void writeError( Exception e, int fallimento )
	{
		logWrite( "Fallimento n° " + fallimento + "\n"
		        + "e.getMessage() ==> " + e.getMessage() + "\n"
		        + "e.getLocalizedMessage() ==> " + e.getLocalizedMessage() + "\n"
		        + "e.getStackTrace() ==> " + e.getStackTrace() + "\n" );
	}
	
	public void logWrite( String message )
	{
		BufferedWriter writer = null;
        
		try
        {
            //create a temporary file
            String timeLog = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).format( Calendar.getInstance().getTime() );
            //File logFile = new File(timeLog);
            File logFile = new File( "log.txt" );

            // This will output the full path where the file will be written to...
            System.out.println( logFile.getCanonicalPath() );

            writer = new BufferedWriter( new FileWriter( logFile ) );
            writer.write( message );
            
        }
        catch ( Exception e1 )
        {
            e1.printStackTrace();
        }
        finally
        {
            try
            {
                writer.close();
            }
            catch ( Exception e2 )
            {
            	System.out.println( "Fallimento nella chiusura del FileWriter" );
            	e2.printStackTrace();
            }
        }
	}
}
