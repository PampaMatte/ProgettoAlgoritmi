package DijkstraAlgorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Graph {

	protected static final String EXT = "gph"; // Estensione dei file di input per la generazione dei grafi 
	private int nNodes;
	private ArrayList<MyNode> lNodes = new ArrayList<MyNode>();
	private ArrayList<MyEdge> lEdges = new ArrayList<MyEdge>();
	public String[] NodesName;
	
	
	public Graph( String fileName ) throws IOException  //  Permette di prendere l'intero grafo in input da un file.gph
	{ 
		BufferedReader reader = new BufferedReader( new FileReader( fileName ) );
		String line;
		boolean nodesEnded = false;
		
		while ( ( line = reader.readLine() ) != null )
		{     
	        /**/ 
	        if ( line.equals( "#" ) )
	        	nodesEnded = true;
	        else
	        {
	        	if( !nodesEnded ) // Memorizzo i nodi
	        	{
	        		String[] split = line.split( " " );
	        		insertNode( split[0] );
	        	}
	        	else  // Memorizzo gli archi
	        	{
	        		String[] split = line.split( ";" );
	        		
	        		// Controllo se i valori letti sono tre (se sono di più vengono ignorati)
	        		if ( split.length >= 3 )
	        		{	
	        			// Controllo se i valori letti sono validi
	        			if ( isInteger( split[0] ) && getIndexOf( split[1] ) != -1 && getIndexOf( split[2] ) != -1 )
	        			{
	        				int val = Integer.parseInt( split[0], 10 );
	        				insertEdge( split[1], split[2], val );
	        			}
	        		}	
	        	}
	        }
	    }
		reader.close();
	}
	
	public Graph( boolean isRandom )  // permette di creare i nodi e gli archi random (TRUE) oppure un grafo vuoto (FALSE)
	{
		if( isRandom )
		{
			String name;
			//this.nNodes = ThreadLocalRandom.current().nextInt( Main.MIN_NUM_NODES, Main.MAX_NUM_NODES );
			int numberOfNodes = ThreadLocalRandom.current().nextInt( Main.MIN_NUM_NODES, Main.MAX_NUM_NODES );
			
			//NodesName = new String[this.nNodes];
			//for ( int i = 0; i < this.nNodes; i++ )
			NodesName = new String[numberOfNodes];
			for ( int i = 0; i < numberOfNodes; i++ )
			{
				name = Integer.toString( i + 1 );
				MyNode a = new MyNode( name ); 
				lNodes.add( a );
			}
			
			setNodesName();
		}
		else
		{
			Main.printErrConsole( Main.WARNING_PREFIX, "Generazione grafo vuoto", "", 300 );
			//this.nNodes = 0;
		}
	}
	
	public void generateRandomEdges()
	{
		int n = ThreadLocalRandom.current().nextInt( 1, lNodes.size() - 1 ),
				random1 = 0,
				random2 = 0,
				weight = 0;
		
		String N1, N2;
		
		for( int i = 0; i < n; i++ )
		{
			weight = ThreadLocalRandom.current().nextInt( 1, Main.MAX_WEIGHT );
			//random1 = ThreadLocalRandom.current().nextInt( 1, this.nNodes );
			//random2 = ThreadLocalRandom.current().nextInt( 1, this.nNodes );
			random1 = ThreadLocalRandom.current().nextInt( 1, lNodes.size() );
			random2 = ThreadLocalRandom.current().nextInt( 1, lNodes.size() );
			
			if ( lNodes.size() > 2 )
			{
				while ( random1 == random2 )
				{
					random2 = ThreadLocalRandom.current().nextInt( 1, lNodes.size() );
					System.out.println( "random1 = " + random1 + " random2 = " + random2);
				}
			}

			N1 = Integer.toString( random1 );
			N2 = Integer.toString( random2 );

			MyNode start =  getNodeFromList( N1 ),
				   end = getNodeFromList( N2 );

			//  Controllo se esistono già degli archi con qui nodi
			boolean alreadyPresent = false;
			for ( int k = 0; k < lEdges.size(); k++ )
			{
				if ( ( start.equals( lEdges.get( k ).getStartNode() ) && end.equals( lEdges.get( k ).getEndNode() ) ) || ( end.equals( lEdges.get( k ).getStartNode() ) && start.equals( lEdges.get( k ).getEndNode() ) ) )
						alreadyPresent = true;
			}
			
			if ( !alreadyPresent )
			{
				Main.printErrConsole( Main.NORMAL_PREFIX, "Creato arco da " + start.getName() + " a " + end.getName() + ", con peso " + weight, "", 200 );
				
				MyEdge tmp = new MyEdge( getNodeFromList( N1 ), getNodeFromList( N2 ), weight ); 
				lEdges.add( tmp );
			}
			else
				i--;
		}
	}
	
	/**
	 * Restituisce la posizione di un nodo dato il suo nome
	 * @param nodeName, nome del nodo
	 * @return la posizione del nodo nell'ArrayList, -1 se non è presente
	 */
	public int getIndexOf( String nodeName ) { return lNodes.indexOf( getNodeFromList( nodeName ) ); }
	
	private void setNodesName()
	{
		int size = lNodes.size();
		for(int i = 0; i < size; i++)
		{
			NodesName[i] = lNodes.get(i).getName();
			//System.out.println( "[" + i + "] => " + NodesName[i] );
		}
	}

	public ArrayList<MyNode> getAllNodes() { return this.lNodes; }

	/**
	 * Ritorna l'ArrayList con tutti gli archi
	 * @return la lista degli archi
	 */
	public ArrayList<MyEdge> getAllEdges() { return this.lEdges; }
	
	public ArrayList<MyEdge> getEdgeByNode (MyNode node){
		ArrayList<MyEdge> EdgesList = new ArrayList<MyEdge>();
		for(int i = 0; i < lEdges.size(); i++ ) {
			if(lEdges.get( i ).getStartNode() == node || lEdges.get( i ).getEndNode() == node ) {
				EdgesList.add( lEdges.get( i ) );
			}
		}
		return EdgesList;
	}
	
	/**
	 * Ritorna il nodo del grafo a partire dal suo nome
	 * @param nodeName, nome del nodo
	 * @return il nodo, NULL se non è presente nel grafo
	 */
	public MyNode getNodeFromList( String nodeName )
	{	
		MyNode resultNode = null;
		int size = lNodes.size();
		
		for ( int i = 0; i < size; i++ )
		{	
			MyNode visited = lNodes.get( i );
			if( visited.getName().equals( nodeName ) )
			{
				resultNode = visited;
			}
		}
		return resultNode;
	}
	
	/**
	 * Inserisce un nuovo nodo nel grafo
	 * @param name, nome del nuovo nodo
	 */
	public void insertNode( String nodeName )
	{
		MyNode newNode = new MyNode( nodeName ); 
		lNodes.add( newNode );
		//this.nNodes = lNodes.size();
	}
	
	/**
	 * Permette di eliminare un nodo dal grafo
	 * @param name, nome del nodo da eliminare
	 */
	public void deleteNode( String nodeName )
	{	
		int index = getIndexOf( nodeName );
		//if ( getIndexOf( nodeName ) == -1 )
		if ( index == -1 )
		{
			Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo non esiste", "", 400 );
		}
		else
		{
			lNodes.remove( index );	
			Main.printErrConsole( Main.NORMAL_PREFIX, "Il nodo " + nodeName + " è stato eliminato correttamente", " ", 200 );
		}
		
		//int nodesNumber = lNodes.size();
		
		/*if ( this.nNodes > nodesNumber )
			this.nNodes = nodesNumber;*/
	}

	/**
	 * Inserisce un nuovo arco nel grafo
	 * @param nodeNameStart, nome del nodo di partenza
	 * @param nodeNameEnd, nome del nodo di arrivo
	 * @param weight, peso dell'arco
	 */
	public void insertEdge( String nodeNameStart, String nodeNameEnd, int weight )
	{	
		if ( getIndexOf( nodeNameStart ) == -1 )
		{
			Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo di partenza non esiste", "", 400 );
		}
		else if ( getIndexOf( nodeNameEnd ) == -1 )
		{
			Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo di di arrivo non esiste", "", 400 );
		}
		else
		{
			lEdges.add( new MyEdge( getNodeFromList( nodeNameStart ), getNodeFromList( nodeNameEnd ), weight ) );
			Main.printErrConsole( Main.NORMAL_PREFIX, "Creato arco da " + nodeNameStart + " a " + nodeNameEnd + " con peso " + weight, "", 200 );
		}
	}

	/**
	 * Elimina un arco dal grafo
	 * @param nodeNameStart, nome del nodo di partenza
	 * @param nodeNameEnd, nome del nodo di arrivo
	 */
	public void deleteEdge( String nodeNameStart, String nodeNameEnd )
	{	
		if ( getIndexOf( nodeNameStart ) == -1 )
		{
			Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo di partenza non esiste", "", 400 );
		}
		else if ( getIndexOf( nodeNameEnd ) == -1 )
		{
			Main.printErrConsole( Main.ERROR_PREFIX, "Il nodo di di arrivo non esiste", "", 400 );
		}
		else
		{
			MyNode start = getNodeFromList( nodeNameStart ),
			         end = getNodeFromList( nodeNameEnd );
			
			lEdges.remove( getEdge( start, end ) );
			Main.printErrConsole( Main.NORMAL_PREFIX, "Eliminato arco da " + nodeNameStart + " a " + nodeNameEnd, " ", 200 );
		}
	}

	/**
	 * Controlla se una string è convertibile in un intero
	 * @param string, la stringa che deve essere convertita in intero
	 * @return true o false se la stringa è convertibile o meno
	 */
	public static boolean isInteger( String string )
	{
	    try
	    { 
	        Integer.parseInt( string ); 
	    }
	    catch( NumberFormatException e )
	    { 
	        return false; 
	    }
	    catch( NullPointerException e )
	    {
	        return false;
	    }
	    
	    return true;
	}
	
	public MyEdge getEdge ( MyNode n1, MyNode n2 ) {
		
		MyEdge tmp = null;
		
		for (int i = 0; i < this.lEdges.size(); i++ ) {
			tmp = this.lEdges.get( i );
			if ( (tmp.getStartNode() == n1 && tmp.getEndNode() == n2 ) || (tmp.getStartNode() == n2 && tmp.getEndNode() == n1 ))
				return tmp;
		}
		
		return null;
	}
}