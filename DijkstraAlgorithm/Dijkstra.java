package DijkstraAlgorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Dijkstra {
	/**
	 * Modifica del colore del nodo visitato
	 * 
		MyNode.setStyle( "-fx-background-color: #F44336;"
		               + "-fx-border-width: 3;" 
		               + "-fx-border-radius: 50%;" 
		               + "-fx-border-color: #F44336;" );
	 
	 * Ripristino del colore normale del nodo
	
		MyNode.setStyle( "-fx-background-color: #263238;"
		               + "-fx-border-width: 3;"
		               + "-fx-border-radius: 50%;"
		               + "-fx-border-color: #263238;" );
		               
	 * Percorso finale 
	 
	    MyNode.setStyle( "-fx-background-color: #4CAF50;"
		               + "-fx-border-width: 3;"
		               + "-fx-border-radius: 50%;"
		               + "-fx-border-color: #4CAF50;" );
	 */
	
	private Set < MyNode > visited; //insieme dei nodi visitati
	private Set < MyNode > border; //insieme dei nodi che si possono visitare
	
	private HashMap < MyNode , MyNode > bestPredecessor; //ad ogni nodo è assegnato il miglior predecessore
	private HashMap < MyNode , Integer > distanceToRoot; //distanza dal nodo di partenza di ogni nodo
	
	private MyNode lastNodeVisited; // contiene l'ultimo nodo modificato
	private MyNode start; // nodo di partenza
	private MyNode destination; // nodo di arrivo
	
	private Graph graph;

	private boolean isFinished;
	
	Dijkstra (Graph g, MyNode x, MyNode y){
		
		this.graph = g;
		
		this.bestPredecessor = new HashMap < MyNode , MyNode > ();
		this.distanceToRoot = new HashMap < MyNode , Integer > ();
		
		ArrayList<MyNode> nodes = g.getAllNodes();
		
		for (int i = 0; i < nodes.size(); i++) {
			MyNode n = nodes.get(i);
			bestPredecessor.put( n , null );
			distanceToRoot.put( n , Integer.MAX_VALUE);
		}
		
		this.isFinished = false;
		this.start = x;
		this.destination = y;
		
		this.visited = new HashSet <MyNode>();
		this.border = new HashSet <MyNode> ();
		
		this.border.add(x);
		this.distanceToRoot.put(x,0);
		
		this.lastNodeVisited = null;
		
	}
	
	public HashMap < MyNode , MyNode > getBestPredecessor (){
		return this.bestPredecessor;
	}
	
	public HashMap < MyNode , Integer > getDistanceToRoot (){
		return this.distanceToRoot;
	}
	
	//avanza di un passo l'algoritmo di dijkstra
	public int goForward () {
		
		if (this.isFinished) {
			this.lastNodeVisited = null;
			return 2;
		}
		// prendo il prossimo nodo da visitare
		this.lastNodeVisited = bestBorderNode();
		
		if ( this.lastNodeVisited == null ){
			this.isFinished = true;
			return -1;
		}else if ( this.lastNodeVisited == destination ){
			this.isFinished = true;
			return 1;
		}
		//funzione che mi da tutti gli archi del nodo
		ArrayList<MyEdge> linkedEdge = graph.getEdgeByNode( this.lastNodeVisited );
		//ad ogni nodo adiacente vengono modificati i dati bestPredecessor e distanceToRoot
		//in questo modo: 
		
		int weight; //peso dell'arco
		MyNode adj;
		for ( int i = 0 ; i < linkedEdge.size() ; i++ ) {
			weight = linkedEdge.get( i ).getWeight();
			adj = linkedEdge.get( i ).getAdjNodeByEdge( this.lastNodeVisited );
			if (!visited.contains( adj )) { //controllo che non sia già stato visitato
				border.add( adj );
				//aggiorno gli array distancetoroot e bestpredecessor
				if ((distanceToRoot.get( this.lastNodeVisited ) + weight ) < distanceToRoot.get( adj )) {
					distanceToRoot.put( adj , distanceToRoot.get( this.lastNodeVisited ) + weight );
					bestPredecessor.put( adj , this.lastNodeVisited );
				}
			}
		}
		return 0;
	}
	
	public MyNode bestBorderNode () {
		if ( border.size() != 0 ) {
			MyNode bestNode;
			MyNode[] nodes = border.toArray( new MyNode[border.size()] );
		
			bestNode = nodes[0];
			for ( int i = 1; i < nodes.length; i++ ) {
				if ( distanceToRoot.get(nodes[i]) < distanceToRoot.get( bestNode ) )
					bestNode = nodes[i];
			}
		
			border.remove( bestNode );
			visited.add( bestNode );
		
			return bestNode;
		}
		return null;
	}

	public Graph getGraph () {
		return this.graph;
	}

	public boolean isFinished (){
		return this.isFinished;
	}
	
	public MyNode getLastNodeVisited () {
		return this.lastNodeVisited;
	}
	
	public MyEdge getLastEdgeVisited () {
		
		if ( this.lastNodeVisited == null || this.lastNodeVisited == this.start )
			return null;
		return graph.getEdge ( this.lastNodeVisited, bestPredecessor.get( this.lastNodeVisited ) );
	}
	
	public MyNode getStart () {
		return this.start;
	}
	
	public MyNode getDestination () {
		return this.destination;
	}
	
	//ritorna il miglior percorso che l'agoritmo di dijkstra ha trovato
	public ArrayList< MyNode > getBestPath () {
	
		ArrayList< MyNode > tmp = new ArrayList< MyNode >();
		
		MyNode prevNode = this.bestPredecessor.get( this.destination );
		tmp.add( this.destination );
		tmp.add( prevNode );
		
		while ( this.start != prevNode ) {
			
			prevNode = this.bestPredecessor.get( prevNode );
			tmp.add( prevNode );

		}
		
		tmp.add( this.start );
		return tmp;
		
	}
	
}


