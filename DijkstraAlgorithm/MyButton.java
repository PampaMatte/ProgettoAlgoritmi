package DijkstraAlgorithm;

import javafx.scene.control.Button;

public class MyButton extends Button
{
	public MyButton( String label )
	{
		super( label );
		setId( "MyButton" );
	}
}
