package DijkstraAlgorithm;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.shape.Line;
import javafx.geometry.Bounds;
import javafx.scene.control.Tooltip;

public class MyEdge
{
	private static final double WEIGHT_PADDING = 1.0/*2.5*/;
	private static final double OFFSET = 2.5;

	private MyNode start,
				   end;

	private Line line;
	private Label weightLabel;
	private int weight;

	public MyEdge( MyNode start, MyNode end, int weight )
	{
		super();
		setStartNode( start );
		setEndNode( end );

		line = new Line();
		line.setId( "MyLine" );
		setLine( line );
		setWeight( weight );

		Label tmp = new Label( Integer.toString( weight ) );
		tmp.setId( "weight" );
		setWeightLabel( tmp );
/*
		Bounds b1 = start.localToScene( start.getBoundsInLocal() );
		Bounds b2 = end.localToScene( end.getBoundsInLocal() );

		double startX = b1.getMinX(),
			   startY = b1.getMinY(),
			   endX = b2.getMinX(),
			   endY = b2.getMinY();
*/

		tmp.setPrefWidth( 20 );
		tmp.setPrefHeight( 20 );

		Tooltip tp = new Tooltip( Integer.toString( weight ) );
		Tooltip.install( line, tp );

		repaint();
	}

	protected void repaint()
	{
		Bounds b1 = getStartNode().localToScene( getStartNode().getBoundsInLocal() ),
			   b2 = getEndNode().localToScene( getEndNode().getBoundsInLocal() );

		double size = getStartNode().getPrefWidth(),
			   startX = Math.round( b1.getMinX() ),
			   startY = Math.round( b1.getMinY() ),
			   endX = Math.round( b2.getMinX() ),
			   endY = Math.round( b2.getMinY() ),
			   //labelX = Math.abs( ( endX - startX ) ) / 2,
			   //labelY = Math.abs( ( startY - endY ) ) / 2;
			   labelX = startX + ( ( ( endX - startX ) / 2 ) ),
			   labelY = startY + ( ( ( endY - startY ) / 2 ) );
/*		
		if ( endX == startX )
			labelX -= WEIGHT_PADDING;
		else if ( endY == startY )
			labelY -= WEIGHT_PADDING;
		if ( endX > startX && endY > startY )
		{
			labelX += WEIGHT_PADDING;
			labelY -= WEIGHT_PADDING;
		}
		else if ( endX < startX && endY > startY )
		{
			labelX -= WEIGHT_PADDING;
			labelY -= WEIGHT_PADDING;
		}
		else if ( endX > startX && endY < startY )
		{
			labelX += WEIGHT_PADDING;
			labelY += WEIGHT_PADDING;
		}
		else if ( endX > startX && endY > startY )
		{
			labelX += OFFSET;
		}
		else if ( endX < startX && endY < startY )
		{
			labelX += WEIGHT_PADDING;
			labelY -= WEIGHT_PADDING;
		}
*/
		getWeightLabel().relocate( labelX, labelY );
		
		startX += ( size / 2 );
		startY += ( size / 2 );
		endX += ( size / 2 );
		endY += ( size / 2 );

		getLine().setStartX( startX ); 
		getLine().setStartY( startY ); 
		getLine().setEndX( endX ); 
		getLine().setEndY( endY );
	}

	public void setLine( Line line )          { this.line = line; }
	public void setWeightLabel( Label label ) { this.weightLabel = label; }
	public void setWeight( int weight )       { this.weight = weight; }
	public void setStartNode ( MyNode start ) { this.start = start; }
	public void setEndNode( MyNode end )      { this.end = end; }

	public Label getWeightLabel() { return this.weightLabel; }
	public Line getLine()         { return this.line; }
	public int getWeight()        { return this.weight;}
	public MyNode getStartNode()  { return this.start; }
	public MyNode getEndNode()    { return this.end; }
	
	public MyNode getAdjNodeByEdge ( MyNode node ) {
		
		if ( node == this.start )
			return this.end;
		
		return this.start;
		
	}
}
